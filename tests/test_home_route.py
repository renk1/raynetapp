import pytest
from unittest.mock import patch
from app.main import app

@pytest.fixture
def client():
    app.config['TESTING'] = True
    with app.test_client() as client:
        yield client

@patch('app.main.redis.incr')
@patch('app.main.get_visits', return_value=[{'visit_time': '2022-01-01 12:00:00'}])
@patch('app.main.save_visit')
def test_index_route(mock_save_visit, mock_get_visits, mock_redis_incr, client):
    mock_redis_incr.return_value = 1

    response = client.get('/')
    
    assert response.status_code == 200
    mock_save_visit.assert_called_once()
    mock_redis_incr.assert_called_once_with('visits')