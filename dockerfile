FROM python:3.10.8-slim-buster
ADD . /code
COPY init_db.sql /docker-entrypoint-initdb.d/
WORKDIR /code
RUN pip install -r requirements.txt
EXPOSE 5000
ENV FLASK_APP=app/main.py
CMD ["python", "run.py"]