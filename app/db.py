import psycopg2
import os
from datetime import datetime
from app.variables.vars import DB_HOST, DB_NAME, DB_PASS, DB_PORT, DB_USER

def get_db_connection():
    conn = psycopg2.connect(
        database=DB_NAME,
        user=DB_USER,
        password=DB_PASS,
        host=DB_HOST,
        port=DB_PORT
    )
    return conn

def save_visit():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("INSERT INTO visits (visit_time) VALUES (%s)", (datetime.now(),))
    conn.commit()
    cur.close()
    conn.close()

def get_visits():
    conn = get_db_connection()
    cur = conn.cursor()
    cur.execute("SELECT * FROM visits")
    visits = cur.fetchall()
    cur.close()
    conn.close()
    return visits
