from flask import Flask

app = Flask(__name__)

# Import routes to ensure they're registered with the app
from . import main
