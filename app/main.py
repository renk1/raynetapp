# app/main.py
from flask import Flask, jsonify
from .db import save_visit, get_visits
from redis import Redis
import os

app = Flask(__name__)
redis = Redis(host='redis', port=6379, db=0, decode_responses=True)

@app.route('/')
def index():
    # Increment visit count in Redis
    visits = redis.incr('visits')

    # Save the visit to PostgreSQL
    save_visit()

    # Get all visits from PostgreSQL
    all_visits = get_visits()

    return jsonify({
        'message': 'Welcome to the HTTP app!',
        'visits': visits,
        'all_visits': all_visits
    })
